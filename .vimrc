colorscheme simple-dark

syntax enable

set number

set showcmd

set cursorline

filetype indent on

set wildmenu

set showmatch
